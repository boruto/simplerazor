﻿/// <binding AfterBuild='copy' ProjectOpened='watch' />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require("gulp"),
    less = require("gulp-less")

gulp.task('copy', function () {
    gulp.src("./bower_components/bootstrap/dist/**").pipe(gulp.dest("./wwwroot/lib/bootstrap"));
    gulp.src("./bower_components/angular/angular*.{js,map}").pipe(gulp.dest("./wwwroot/lib/angular"));
    gulp.src("./bower_components/react/react*.js").pipe(gulp.dest("./wwwroot/lib/react"));
});

gulp.task("less", function () {
    gulp.src("./wwwroot/less/*.less").pipe(less({ compress: true })).pipe(gulp.dest("./wwwroot/less"));
});

gulp.task("watch", function () {
    gulp.watch("./wwwroot/less/*.less", ["less"]);
})